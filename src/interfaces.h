#pragma once

#include "IUObject.h"
#include "resolve.h"

template<class Vector>
class Object {
    virtual ~Object() = default;
};
template<class Vector>
class Movable : public Object<Vector> {
public:
    virtual Vector getPosition() const = 0;

    virtual void setPosition(Vector const &newValue) = 0;

    virtual Vector getVelocity() const = 0;

    virtual ~Movable() = default;
};

template <class Vector>
class MovableAdapter : public Movable<Vector>
{
    private:
    IUObject &m_obj;
    public:
    MovableAdapter(IUObject &obj): m_obj(obj) {}
    Vector getPosition() const {
        return resolve<Vector>("position", m_obj);
    };
    void setPosition(Vector const &newValue){
        resolve<Vector>("position", m_obj, newValue);
    };
    Vector getVelocity() const {
        return resolve<Vector>("velocity", m_obj);
    };
    
    ~MovableAdapter() {}
};

template<class Vector>
class Fuelable : public Object<Vector> {
public:
    virtual Vector getFuelLevel() const = 0;

    virtual void setFuelLevel(Vector const &newValue) = 0;

    virtual ~Fuelable() = default;
};

template <class Vector>
class FuelableAdapter : public Fuelable<Vector>
{
    private:
    IUObject &m_obj;
    public:
    FuelableAdapter(IUObject &obj): m_obj(obj) {}
    Vector getFuelLevel() const {
        return resolve<Vector>("fuel_level", m_obj);
    };
    void setFuelLevel(Vector const &newValue){
        resolve<Vector>("fuel_level", m_obj, newValue);
    };
    
    ~FuelableAdapter() {}
};


