#pragma once

#include <string>
#include <any>
#include <map>

class IUObject
{
    public:
    virtual std::any &operator[](const std::string &key) = 0;
    virtual ~IUObject() = default;
};

class UObject : public IUObject
{
    private:
    std::map<std:: string, std::any> properties;
    public:
    UObject();
    std::any &operator[](const std::string &key);
    ~UObject();
};

/*
template <class Vector>
class UObject 
{
    std::map<std::string, Object<Vector>> list_;
    public:
    UObject(std::map<std::string, Object<Vector>> list) : list_(list) {} 
    Object<Vector> get(std::string name)
    {
        return list_[name];
    }

};*/